import React from "react";
import product from "../Product";

const Description = () => {
  return (
    <>
      <p>
        Description du produit :{" "}
        <span className="lead">{product.description}</span>
      </p>
    </>
  );
};

export default Description;
