import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Prix from "./components/Prix";
import Nom from "./components/Nom";
import Description from "./components/Description";
import Image from "./components/Image";

const App = () => {
  let firstName = prompt("Enter your name");
  return (
    <>
      <Card className="mt-5 ms-4" style={{ width: "18rem" }}>
        <Card.Img
          variant="top"
          src="/images/logo.png"
          width="50%"
          height="200px"
        />
        <Card.Body>
          <Card.Title>
            <Nom></Nom>
            <span className="lead">
              <Prix></Prix>
            </span>
          </Card.Title>
          <Card.Text>
            <Description></Description>
          </Card.Text>
          <Button variant="primary">Go somewhere</Button>
        </Card.Body>
      </Card>

      <p className="mt-3">
        {firstName ? `Bonjour ${firstName}` : "Hello there"}
      </p>
      {firstName && <img src="/images/logo.png" alt="Image" />}
    </>
  );
};

export default App;
